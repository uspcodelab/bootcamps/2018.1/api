class V1::UserOwnsLocker < ApplicationRecord
  belongs_to :v1_user, optional: true
  belongs_to :v1_locker, optional: true
end

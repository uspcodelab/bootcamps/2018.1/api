class V1::RequestsController < ApplicationController
  before_action :set_v1_request, only: [:show, :update, :destroy]

  # GET /v1/requests
  def index
    @v1_requests = V1::Request.all

    render json: @v1_requests
  end

  # GET /v1/requests/1
  def show
    render json: @v1_request
  end

  # POST /v1/requests
  def create
    @v1_request = V1::Request.new(v1_request_params)

    if @v1_request.save
      render json: @v1_request, status: :created, location: @v1_request
    else
      render json: @v1_request.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /v1/requests/1
  def update
    if @v1_request.update(v1_request_params)
      render json: @v1_request
    else
      render json: @v1_request.errors, status: :unprocessable_entity
    end
  end

  # DELETE /v1/requests/1
  def destroy
    @v1_request.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_v1_request
      @v1_request = V1::Request.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def v1_request_params
      params.require(:v1_request).permit(:renew, :v1_contract_type_id)
    end
end

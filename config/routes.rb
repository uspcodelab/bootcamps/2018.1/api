Rails.application.routes.draw do
  namespace :v1 do
    resources :user_flow
  end
  namespace :v1 do
    resources :requests
  end
  namespace :v1 do
    resources :features do
    	member do
    		get 'position'
    	end	

      member do
        get 'place'
      end
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

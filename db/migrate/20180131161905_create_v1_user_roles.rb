class CreateV1UserRoles < ActiveRecord::Migration[5.2]
  def change
    create_table :v1_user_roles do |t|
      t.string :user_role

      t.timestamps
    end
  end
end

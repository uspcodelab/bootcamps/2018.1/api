class CreateV1Features < ActiveRecord::Migration[5.2]
  def change
    create_table :v1_features do |t|
      t.references :v1_position, foreign_key: true
      t.references :v1_place, foreign_key: true

      t.timestamps
    end
  end
end

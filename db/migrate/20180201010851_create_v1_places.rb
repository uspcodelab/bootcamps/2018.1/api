class CreateV1Places < ActiveRecord::Migration[5.2]
  def change
    create_table :v1_places do |t|
      t.string :place

      t.timestamps
    end
    add_index :v1_places, :place, unique: true
  end
end

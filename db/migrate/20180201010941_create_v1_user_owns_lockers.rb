class CreateV1UserOwnsLockers < ActiveRecord::Migration[5.2]
  def change
    create_table :v1_user_owns_lockers do |t|
      t.references :v1_user, foreign_key: true
      t.references :v1_locker, foreign_key: true
      t.datetime :start_date
      t.datetime :end_date

      t.timestamps
    end
  end
end

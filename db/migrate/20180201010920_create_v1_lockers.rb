class CreateV1Lockers < ActiveRecord::Migration[5.2]
  def change
    create_table :v1_lockers do |t|
      t.string :number
      t.boolean :available
      t.boolean :deactivated

      t.timestamps
    end
    add_index :v1_lockers, :number, unique: true
  end
end

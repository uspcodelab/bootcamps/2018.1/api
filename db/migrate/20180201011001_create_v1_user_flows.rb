class CreateV1UserFlows < ActiveRecord::Migration[5.2]
  def change
    create_table :v1_user_flows do |t|
      t.references :v1_user, foreign_key: true
      t.references :v1_locker, foreign_key: true
      t.datetime :reserve_end_date
      t.boolean :reserve_payment
      t.references :v1_user_reserves_locker, foreign_key: true
      t.datetime :ownership_end_date
      t.references :v1_user_owns_locker, foreign_key: true
      t.references :v1_status, foreign_key: true

      t.timestamps
    end
  end
end
